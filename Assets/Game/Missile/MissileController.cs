﻿using Game.Player;
using UnityEngine;

namespace Game.Missile
{
    public class MissileController : MonoBehaviour
    {
        private const float OutOfBounds = 10000f;
        private const float MaxLifetime = 5f;

        public float speed = 200f;

        //without rigidbody continuous collision detection fast projectiles fly thought objects
        private Rigidbody _rigidbody;
        private float _lifetime = 0f;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            _lifetime += Time.deltaTime;
            if (transform.position.magnitude > OutOfBounds || _lifetime > MaxLifetime)
            {
                Destroy(gameObject);
            }
        }

        private void FixedUpdate()
        {
            var velocity = transform.forward * speed;
            _rigidbody.velocity = velocity;
        }

        private void OnCollisionEnter(Collision other)
        {
            Debug.Log($"Missile collided with: {other.gameObject.name}");
            if (other.gameObject.tag.Equals("Plane"))
            {
                var plane = other.gameObject.GetComponent<PlaneController>();
                plane.Explode();
            }
            Destroy(gameObject);
        }
    }
}
