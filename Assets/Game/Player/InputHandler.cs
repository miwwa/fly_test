﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Player
{
    [RequireComponent(typeof(PlaneController))]
    public class InputHandler : MonoBehaviour
    {
        private GameActions _actions;
        private PlaneController _plane;

        private void Awake()
        {
            _plane = GetComponent<PlaneController>();
            _actions = new GameActions();
            _actions.Enable();
        }

        private void OnEnable()
        {
            _actions.Player.Boost.started += OnBoostStarted;
            _actions.Player.Boost.canceled += OnBoostCancelled;
        }

        private void OnDisable()
        {
            _actions.Player.Boost.started -= OnBoostStarted;
            _actions.Player.Boost.canceled -= OnBoostCancelled;
        }

        private void Update()
        {
            if (_actions.Player.MoveX.phase == InputActionPhase.Started)
            {
                var x = _actions.Player.MoveX.ReadValue<float>();
                _plane.Roll(x);
            }
            if (_actions.Player.MoveY.phase == InputActionPhase.Started)
            {
                var y = _actions.Player.MoveY.ReadValue<float>();
                _plane.Pitch(y);
            }

            if (_actions.Player.Fire.triggered)
            {
                _plane.Fire();
            }
        }

        private void OnBoostStarted(InputAction.CallbackContext obj)
        {
            _plane.StartBoost();
        }

        private void OnBoostCancelled(InputAction.CallbackContext obj)
        {
            _plane.EndBoost();
        }
    }
}
