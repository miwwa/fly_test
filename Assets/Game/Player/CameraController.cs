﻿using UnityEngine;

namespace Game.Player
{
    public class CameraController : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset;
        [Range(0.01f, 1f)]
        public float speed = 0.1f;

        private void Update()
        {
            if (target == null)
            {
                return;
            }

            var targetPosition = target.TransformPoint(offset);
            transform.position = Vector3.Lerp(transform.position, targetPosition, speed);
            transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, speed);
        }
    }
}
