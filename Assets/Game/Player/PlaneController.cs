﻿using System;
using UnityEngine;

namespace Game.Player
{
    public class PlaneController : MonoBehaviour
    {
        public float thrustSpeed = 10f;
        public float boostSpeed = 20f;
        public float rollSpeed = 90f;
        public float pitchSpeed = 90f;
        public float yawSpeed = 90f;

        [Space]
        public GameObject missilePrefab;
        public Transform missileSpawnPoint;

        public bool IsBoosting { get; private set; }

        public event Action<PlaneController> OnDestroy;
        private bool _destroyed = false;

        private void Awake()
        {
            if (missilePrefab == null)
            {
                throw new NullReferenceException("Set missile prefab!");
            }
            if (missileSpawnPoint == null)
            {
                throw new NullReferenceException("Set missile spawn point!");
            }
        }

        private void Update()
        {
            var speed = IsBoosting ? boostSpeed : thrustSpeed;
            var velocity = Vector3.forward * speed * Time.deltaTime;
            transform.Translate(velocity, Space.Self);
        }

        private void OnCollisionEnter(Collision other)
        {
            Explode();
        }

        public void Pitch(float pitch)
        {
            float angle = pitch * pitchSpeed * Time.deltaTime;
            transform.Rotate(transform.right, angle, Space.World);
        }

        public void Roll(float roll)
        {
            float angle = roll * rollSpeed * Time.deltaTime;
            transform.Rotate(-transform.forward, angle, Space.World);
        }

        public void Yaw(float yaw)
        {
            float angle = yaw * yawSpeed * Time.deltaTime;
            transform.Rotate(transform.up, angle, Space.World);
        }

        public void StartBoost()
        {
            IsBoosting = true;
        }

        public void EndBoost()
        {
            IsBoosting = false;
        }

        public void Fire()
        {
            Instantiate(missilePrefab, missileSpawnPoint.position, transform.rotation);
        }

        public void Explode()
        {
            if (!_destroyed)
            {
                _destroyed = true;
                OnDestroy?.Invoke(this);
                Destroy(gameObject);
            }
        }
    }
}
