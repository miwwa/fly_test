﻿using Game.Player;
using UnityEngine;

namespace Game.Enemy
{
    [RequireComponent(typeof(PlaneController))]
    public class EnemyMoveAroundAI : MonoBehaviour
    {
        public float speed = -0.5f;

        public PlaneController Plane { get; private set; }

        private void Awake()
        {
            Plane = GetComponent<PlaneController>();
        }

        private void Update()
        {
            Plane.Yaw(speed);
        }
    }
}
