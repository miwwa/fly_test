﻿using Game.Player;
using UnityEngine;

namespace Game.Enemy
{
    [RequireComponent(typeof(PlaneController))]
    public class ShootAI : MonoBehaviour
    {
        public int shootsCount = 3;
        public float fireRate = 0.2f;
        public float cooldown = 1f;

        private int _shoots;
        private float _fireRate;
        private float _cooldown;

        private PlaneController _plane;

        private void Awake()
        {
            _plane = GetComponent<PlaneController>();
        }

        private void Update()
        {
            _cooldown -= Time.deltaTime;

            if (_shoots > 0)
            {
                _fireRate -= Time.deltaTime;
                if (_fireRate <= 0)
                {
                    _plane.Fire();
                    _fireRate += fireRate;
                    _shoots--;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("Plane") && _cooldown <= 0)
            {
                _shoots = shootsCount;
                _cooldown = cooldown;
            }
        }
    }
}
