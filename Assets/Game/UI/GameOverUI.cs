﻿using Game.Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Game.UI
{
    public class GameOverUI : MonoBehaviour
    {
        public PlaneController player;
        public GameObject gameOverUI;

        private void OnEnable()
        {
            player.OnDestroy += ShowGameOver;
        }

        private void OnDisable()
        {
            player.OnDestroy -= ShowGameOver;
        }

        private void Update()
        {
            if (Keyboard.current.escapeKey.isPressed)
            {
                Application.Quit();
            }
        }

        private void ShowGameOver(PlaneController plane)
        {
            gameOverUI.SetActive(true);
        }

        public void Restart()
        {
            SceneManager.LoadScene(0);
        }
    }
}
