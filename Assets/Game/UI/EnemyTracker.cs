﻿using System.Collections.Generic;
using System.Linq;
using Game.Enemy;
using Game.Player;
using UnityEngine;

namespace Game.UI
{
    public class EnemyTracker : MonoBehaviour
    {
        public GameObject distanceTrackerPrefab;
        public Transform player;

        [Space]
        public float screenBorderOffset = 100f;

        private Camera _camera;
        private RectTransform _transform;
        private List<PlaneController> _enemies;
        private List<DistanceUI> _uiElements;

        private void Start()
        {
            _transform = GetComponent<RectTransform>();
            _camera = Camera.main;
            _enemies = FindObjectsOfType<EnemyMoveAroundAI>().Select(x => x.Plane).ToList();
            _uiElements = new List<DistanceUI>();
            foreach (var enemy in _enemies)
            {
                var uiElement = Instantiate(distanceTrackerPrefab, transform).GetComponent<DistanceUI>();
                _uiElements.Add(uiElement);
                enemy.OnDestroy += RemoveTracker;
            }
        }

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            for (var i = 0; i < _enemies.Count; i++)
            {
                var direction = _enemies[i].transform.position - player.position;
                var screenPoint = _camera.WorldToScreenPoint(_enemies[i].transform.position);
                if (_camera.pixelRect.Contains(screenPoint))
                {
                    var v = Vector3.Dot(_camera.transform.forward, direction);
                    if (v > 0)
                    {
                        // enemy on front of camera
                        var distance = direction.magnitude;
                        RectTransformUtility.ScreenPointToLocalPointInRectangle(_transform, screenPoint, _camera,
                            out var rectTransformPosition);
                        _uiElements[i].ShowText(rectTransformPosition, distance);
                    }
                    else
                    {
                        // enemy behind of camera
                        RectTransformUtility.ScreenPointToLocalPointInRectangle(_transform, screenPoint, _camera,
                            out var rectTransformPosition);
                        var size = _transform.rect.size * 0.5f - new Vector2(screenBorderOffset, screenBorderOffset);
                        rectTransformPosition.x = size.x * Mathf.Sign(rectTransformPosition.x);
                        rectTransformPosition.y = size.y * Mathf.Sign(rectTransformPosition.y);
                        _uiElements[i].ShowIcon(rectTransformPosition);
                    }
                }
                else
                {
                    screenPoint.x = Mathf.Clamp(screenPoint.x, screenBorderOffset,
                        _camera.pixelWidth - screenBorderOffset);
                    screenPoint.y = Mathf.Clamp(screenPoint.y, screenBorderOffset,
                        _camera.pixelHeight - screenBorderOffset);
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(_transform, screenPoint, _camera,
                        out var rectTransformPosition);
                    _uiElements[i].ShowIcon(rectTransformPosition);
                }
            }
        }

        private void OnDestroy()
        {
            foreach (var enemy in _enemies)
            {
                enemy.OnDestroy -= RemoveTracker;
            }
        }

        private void RemoveTracker(PlaneController enemy)
        {
            _enemies.Remove(enemy);
            Destroy(_uiElements[0].gameObject);
            _uiElements.RemoveAt(0);
        }
    }
}
