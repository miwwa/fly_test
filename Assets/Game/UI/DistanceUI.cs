﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class DistanceUI : MonoBehaviour
    {
        private RectTransform _transform;
        private Text _text;
        private Image _image;

        private const string DistanceFormat = "F0";

        private void Start()
        {
            _transform = GetComponent<RectTransform>();
            _text = GetComponentInChildren<Text>();
            _image = GetComponentInChildren<Image>();
        }

        public void ShowText(Vector2 position, float distance)
        {
            _transform.anchoredPosition = position;
            _text.text = distance.ToString(DistanceFormat);
            _text.enabled = true;
            _image.enabled = false;
        }

        public void ShowIcon(Vector2 position)
        {
            _transform.anchoredPosition = position;
            _text.enabled = false;
            _image.enabled = true;
        }
    }
}
